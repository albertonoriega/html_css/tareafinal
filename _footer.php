    <!-- Comienzo del footer-->
    <footer>
        <ul class="socials">
            <li>
                Contáctanos
            </li>
            <li>
                <span class="facebook">
                    <a href="https://es-es.facebook.com/" target="_blank"><ion-icon name="logo-facebook"></ion-icon></a>
                </span>
            </li>
            <li>
                <span class="twitter">
                    <a href="https://twitter.com/home" target="_blank"><ion-icon name="logo-twitter"></ion-icon></a>
                </span>
            </li>
            <li>
                <span class="instagram">
                    <a href="https://www.instagram.com/" target="_blank"><ion-icon name="logo-instagram"></ion-icon></a>
                </span>
            </li>
            <li>
                <span class="email">
                    <a href="https://www.google.com/intl/es/gmail/about/" target="_blank"><ion-icon name="mail-outline"></ion-icon></a>
                </span>
            </li>
        </ul>
    </footer>
    <!-- Final del footer-->