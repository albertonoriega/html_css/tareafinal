<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="estilos.css">
    <style>

    </style>
</head>

<body>
    <!-- Cargamos el menu-->
    <?php
    require_once '_menu.php';
    ?>

    <!-- Comienzo del contenido del index-->
    <div class="container-fluid mb-5">

        <!-- Tabla de BS con dos columnas-->
        <div class="row">
            <!-- Primera columna-->
            <div class=" col-xl-8  mt-5">
                <p class="my-5 text-justify">El Teniente Blueberry (o simplemente Blueberry) es una serie francesa de
                    historietas del oeste iniciada en 1963 por el guionista Jean-Michel Charlier y el dibujante Jean
                    Giraud para la revista Pilote que narra las aventuras del Teniente de Caballería Mike Steve Donovan,
                    alias "Blueberry", en sus viajes por el Viejo Oeste de los Estados Unidos. Blueberry es un héroe del
                    wéstern atípico: no es un hombre de la ley que cabalga errante llevando a los malhechores a la
                    justicia, ni un vaquero apuesto que entre cabalgando a un pueblo, salve el rancho, sea nombrado
                    el nuevo sheriff y se case con la maestra de la escuela. En cualquier situación, ve lo que cree
                    tiene que hacer y lo hace.
                </p>

                <!-- Inicio del carousel-->
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="imgs/1.png" height="600" class="img-fluid" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="imgs/4.jpg" height="600" class="img-fluid" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="imgs/5.png" height="600" class="img-fluid" alt="Third slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="imgs/3.png" height="600" class="img-fluid" alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

                <script>
                    $('.carousel').carousel({
                        interval: 3500
                    })
                </script>

                <!-- Fin del carousel-->

                <h2 class="mt-5">Argumento</h2>
                <p class="mt-3 text-justify">
                    En 1861 Mike Donovan, hijo de un hacendado sudista de Georgia, es acusado de asesinar al padre de su
                    prometida, Harriet Tucker. Perseguido por el auténtico asesino, un esclavo huido le ayuda a pasar a
                    las
                    líneas nordistas en el preciso momento de estallar la Guerra de Secesión. Adopta el nombre de Mike
                    S.
                    Blueberry y pasa a ser corneta del regimiento de caballería. Pendenciero, aficionado al juego, al
                    alcohol y
                    a las mujeres, es un militar íntegro, audaz y con sentido de la estrategia que asciende hasta ser
                    nombrado
                    Teniente al final de la Guerra de Secesión.
                </p>
                <p class="text-justify">
                    Las aventuras del Teniente le llevan a conocer y salvar la vida del General nordista Dodge durante
                    la
                    guerra. Éste le devolverá el favor más tarde, intercediendo por Blueberry ante el presidente Ulisses
                    S.
                    Grant. Blueberry vivirá un tiempo entre los apaches donde se le conoce como Tsi-Na-Pah (Nariz Rota)
                    y
                    llegará a contar con la amistad del Gran Jefe Cochise y el amor de su hija, Chini. Así mismo, conoce
                    y
                    se
                    enfrenta al mayor Chamán de Guerra apache, Gokhlayeh, más tarde conocido como Gerónimo, en una
                    aventura
                    que
                    le lleva a Tombstone donde coincide con Wyatt Earp y Doc Holliday durante los acontecimientos del OK
                    Corral.

                    Entre los personajes femeninos que aparecen en las aventuras de Blueberry, hay una que es la única
                    que
                    le
                    obsesiona: la cabaretera conocida como Chihuahua Pearl (también conocida como Lilly Calloway en
                    otros
                    tomos).
                </p>
            </div>
            <!-- Segunda columna-->
            <div class="d-none d-xl-flex col-xl-2 offset-xl-1">
                <div class="mt-5 d-flex flex-column align-items-center">
                    <h2 class="mt-5 text-center">Últimos ejemplares</h2>
                    <img src="imgs/Blueberry/0_RencorApache.jpeg" class="img-thumbnail my-5" alt="">
                    <p class="font-weight-bold text-center">Rencor apache (2020)</p>
                    <img src="imgs/Juventud/54_ElConvoyDeLosForajidos.jpeg" class="img-thumbnail my-5" alt="">
                    <p class="font-weight-bold text-center">El convoy de los forajidos (2016)</p>
                    <img src="imgs/Juventud/53_Gettysburg.jpeg" class="img-thumbnail my-5" alt="">
                    <p class="font-weight-bold text-center">Gettysburg (2013)</p>
                </div>
            </div>
        </div>
    </div>

    <!-- Final del contenido del index-->

    <!-- Cargamos el footer-->
    <?php
    require_once '_footer.php';
    ?>

</body>

</html>