<?php
require('blueberryArray.php');
list($blueberry, $juventud) = arrayBlueberry();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <!-- Cargamos el menu-->
    <?php
    require_once '_menu.php';
    ?>

    <?php
    // Si se ha pulsado el bton de detalles de la tabla con las carátulas
    $numero = null;
    if (isset($_GET["detalles"])) {
        $numero = intval($_GET["numero"]);
    }
    // Si se ha pulsado el botón buscar de las lista desplegable
    if (isset($_GET["buscar"])) {
        $numero = intval($_GET["libro"]);
    }
    ?>
    <!-- Comienzo de las migas-->
    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
            <li class="breadcrumb-item"><a href="blueberry.php">Colección Blueberry</a></li>
            <li class="breadcrumb-item active" aria-current="page"> <?= $blueberry[$numero]["titulo"] ?></li>
        </ol>
    </nav>

    <!-- Fin de las migas-->

    <!-- Comienzo de la muestra de datos del libro seleccionado-->

    <div class="contenedorDetalles">
        <div>
            <img src=' <?= $blueberry[$numero]["src"] ?>' width="300px" height="400px" class="img-fluid img-thumbnail" alt="">
        </div>
        <div>
            <ul class="detalles">
                <li>
                    <h2> <?= $blueberry[$numero]["numero"] . " " . $blueberry[$numero]["titulo"] ?></h2>
                </li>
                <li>Autores: <?= $blueberry[$numero]["autores"]; ?></li>
                <li>Fecha: <?= $blueberry[$numero]["fecha"]; ?></li>
                <li>Precio: <?php
                            if ($blueberry[$numero]["descuento"] == 0) {
                                echo $blueberry[$numero]["precio"] . " €";
                            } else {
                                echo "<del>" . $blueberry[$numero]["precio"] . " € </del>";
                                echo $blueberry[$numero]["precio"] * (1 - $blueberry[$numero]["descuento"]) . " € ";

                            ?>
                        <span>Oferta</span>
                    <?php
                            }
                    ?>
                </li>
                <li>Número de páginas: <?= $blueberry[$numero]["paginas"]; ?></li>
            </ul>
        </div>
    </div>
    <!-- Fin de la muestra de datos del libro seleccionado-->

    <!-- Cargamos el footer-->
    <?php
    require_once '_footer.php';
    ?>
</body>

</html>