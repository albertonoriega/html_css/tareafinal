<?php
// Importamos los arrays con los datos
require('blueberryArray.php');
list($blueberry, $juventud) = arrayBlueberry();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="estilos.css">
</head>

<body>
    <!-- Cargamos el menu-->
    <?php
    require_once '_menu.php';
    ?>

    <!-- Comienzo de las migas-->
    <nav style="--bs-breadcrumb-divider: '>';" class="migas" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Colecciones</li>
        </ol>
    </nav>
    <!-- Fin de las migas-->

    <!-- Comienzo del contenedor tabla-->
    <div class="tabla">
        <!-- Comienzo de la tabla1-->

        <h4 class="tabla1"><a class="enlacesTabla" href="blueberry.php">BLUEBERRY</a></h4>

        <h4 class="tabla2"><a class="enlacesTabla" href="juventud.php">JUVENTUD</a></h4>
        <table class="tabla1">
            <tr>
                <th>Nº</th>
                <th>TÍTULO</th>
                <th>AUTORES</th>
            </tr>
            <?php
            for ($i = 0; $i < count($blueberry); $i++) {
            ?>
                <tr>
                    <td> <?= $blueberry[$i]["numero"] ?></td>
                    <td> <?= $blueberry[$i]["titulo"] ?></td>
                    <td> <?= $blueberry[$i]["autores"] ?></td>
                </tr>
            <?php
            }
            ?>
        </table>
        <!-- Fin de la tabla1-->

        <!-- Comienzo de la tabla 2-->
        <table class="tabla2">
            <tr>
                <th>Nº</th>
                <th>Título</th>
                <th>Autores</th>
            </tr>
            <?php
            for ($i = 0; $i < count($juventud); $i++) {
            ?>
                <tr>
                    <td> <?= $juventud[$i]["numero"] ?></td>
                    <td> <?= $juventud[$i]["titulo"] ?></td>
                    <td> <?= $juventud[$i]["autores"] ?></td>
                </tr>
            <?php
            }
            ?>
        </table>
        <!-- Fin de la tabla2-->

    </div>
    <!-- Fin del contenedor tabla-->

    <!-- Cargamos el footer-->
    <?php
    require_once '_footer.php';
    ?>
</body>

</html>