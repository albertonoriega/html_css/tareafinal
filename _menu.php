<!-- Comienzo del navbar-->
<nav class="navbar navbar-expand-xl navbar-light bg-dark d-flex ">
    <a href="index.php"><img class="icono" src="imgs/2.png" width="50px" alt=""></a>
    <p class="text-light">El Teniente Blueberry</p>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse navbar-right" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item ">
                <a class="nav-link text-light" href="colecciones.php">Colecciones <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-light" href="#" role="button" data-toggle="dropdown" aria-expanded="false">
                    Catálogo
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="blueberry.php">Blueberry</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="juventud.php">La juventud de Blueberry</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<!-- Final del navbar-->