<?php
require('blueberryArray.php');
list($blueberry, $juventud) = arrayBlueberry();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="estilos.css">
</head>


<body>
    <!-- Cargamos el menu-->
    <?php
    require_once '_menu.php';
    ?>

    <!-- Comienzo de las migas-->
    <nav style="--bs-breadcrumb-divider: '>';" class="migas" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Colección Blueberry</li>
        </ol>
    </nav>
    <!-- Fin de las migas-->

    <!-- Comienzo del header que contiene una lista desplegable-->
    <header>
        <h1>Colección Blueberry</h1>
        <form action="detallesBlueberry.php">
            <select name="libro" id="libro">
                <option value="">Selecciona libro</option>
                <?php
                for ($i = 0; $i < count($blueberry); $i++) {
                ?>
                    <option value="<?= $i ?>"> <?= $blueberry[$i]["numero"] . " " . $blueberry[$i]["titulo"] ?></option>

                <?php
                }
                ?>
            </select>
            <button class="btn btn-info border border-3 border-dark" name="buscar"><i class="bi bi-search"></i> Buscar</button>
        </form>
    </header>
    <!-- Fin del header-->

    <!-- Comienzo de la tabla que muestra la carátula y el titulo de los libros de la colección Blueberry en forma de tabla -->
    <div class="coleccionBlueberry">
        <?php
        for ($i = 0; $i < count($blueberry); $i++) {
        ?> <form action="detallesBlueberry.php">
                <?php
                echo "<div>";
                echo "<button id='detalles' name='detalles'> <img src='{$blueberry[$i]["src"]}' width='250px' height='300px' ></button>";
                echo "";
                echo "<p>" . $blueberry[$i]["titulo"] . "</p>";
                ?>

                <input type="hidden" name="numero" value=" <?= $i ?> ">
                <button class="btn btn-danger btn-lg btn-block border border-3 border-dark" name="detalles">Ver detalles</button>

            </form>
        <?php
            echo "</div>";
        }
        ?>

    </div>
    <!-- Fin del contenido-->

    <!-- Cargamos el footer-->
    <?php
    require_once '_footer.php';
    ?>

</body>

</html>