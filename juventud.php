<?php
require('blueberryArray.php');
list($blueberry, $juventud) = arrayBlueberry();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="estilos.css">
</head>


<body>
    <!-- Cargamos el menu-->
    <?php
    require_once '_menu.php';
    ?>

    <!-- Comienzo de las migas-->
    <nav style="--bs-breadcrumb-divider: '>';" class="migas" aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page">Colección Juventud Blueberry</li>
        </ol>
    </nav>
    <!-- Fin de las migas-->

    <!-- Comienzo del header que contiene una lista desplegable-->
    <header>
        <h1>Colección Juventud Blueberry</h1>
        <form action="detallesJuventud.php">
            <select name="libroJuventud" id="libro">
                <option value="">Selecciona libro</option>
                <?php
                for ($i = 0; $i < count($juventud); $i++) {
                ?>
                    <option value="<?= $i ?>"> <?= $juventud[$i]["numero"] . " " . $juventud[$i]["titulo"] ?></option>

                <?php
                }
                ?>
            </select>
            <button class="btn btn-info border border-3 border-dark" name="buscarJuventud"><i class="bi bi-search"></i> Buscar</button>
        </form>
    </header>
    <!-- Fin del header-->

    <!-- Comienzo de la tabla que muestra la carátula y el titulo de los libros de la colección Blueberry en forma de tabla -->
    <div class="coleccionBlueberry">
        <?php
        for ($i = 0; $i < count($juventud); $i++) {
        ?>
            <form action="detallesJuventud.php">
                <div>
                    <button id='detalles' name='detallesJuventud'> <img src=' <?= $juventud[$i]["src"] ?>' width='250px' height='300px'> </button>
                    <p> <?= $juventud[$i]["titulo"] ?> </p>

                    <input type="hidden" name="numero" value=" <?= $i ?> ">
                    <button class="btn btn-danger btn-lg btn-block border border-3 border-dark" name="detallesJuventud">Ver detalles</button>

            </form>
    </div>
<?php
        }
?>

</div>
<!-- Fin del contenido-->

<!-- Cargamos el footer-->
<?php
require_once '_footer.php';
?>


</body>

</html>